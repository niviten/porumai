const express = require('express')
const app = express()
const port = 3333

const path = require('path')

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'welcome.html'))
})

const blogs = {
    1: 'item1.html',
    2: 'item2.html',
    3: 'item3.html',
    4: 'item4.html',
    5: 'item5.html'
}
app.get('/blog', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'blog', 'blog.html'))
})
app.get('/blog/:id', (req, res) => {
    if (req.params.id) {
        if (blogs[req.params.id]) {
            res.sendFile(path.join(__dirname, 'pages', 'blog', 'items', blogs[req.params.id]))
            return
        }
    }
    res.sendFile(path.join(__dirname, 'pages', 'blog', 'blog.html'))
})

app.get('/stories', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'stories', 'stories.html'))
})

app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, 'pages', 'contact.html'))
})

app.listen(port, () => {
    console.log(`Server started @ ${port}`)
})
